/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package test_service;

import com.werapan.databaseproject.model.Customer;
import com.werapan.databaseproject.service.CustomerService;

/**
 *
 * @author HotLapTop
 */
public class TestCustomerService {
    public static void main(String[] args) {
        CustomerService cs = new CustomerService();
        for(Customer customer :cs.getCustomers()) {
            System.out.println(customer);
        }
        System.out.println(cs.getByTel("0899999999"));
        Customer cus1 = new Customer("kab", "0899999990");
        cs.addNew(cus1);
        for(Customer customer :cs.getCustomers()) {
            System.out.println(customer);
        }
        Customer delCus = cs.getByTel("0899999990");
        delCus.setTel("0899999999");
        cs.update(delCus);
        System.out.println(delCus);
        for(Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }
        cs.delete(delCus);
        for(Customer customer : cs.getCustomers()) {
            System.out.println(customer);
        }
    }
}
